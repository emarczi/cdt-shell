<#if os == "linux">
<#assign cmd = "su - rr">
</#if>
<#if os == "windows">
<#assign cmd = "/bin/sh -l">
</#if>
#!/bin/bash

docker run --rm -v ${devon_path}:/home/rr -ti devon_base:acgps ${cmd} -c "if [ ! -d /home/rr/ssl ]; then mkdir /home/rr/ssl; fi"
docker run --rm -v ${devon_path}:/home/rr -ti devon_base:acgps ${cmd} -c "openssl req -new -newkey rsa:4096 -days 3650 -nodes -x509 -subj \"/C=WW/ST=YC/L=YC/O=Devon/CN=${domain}\" -keyout /home/rr/ssl/${domain}.key -out /home/rr/ssl/${domain}.pem"
