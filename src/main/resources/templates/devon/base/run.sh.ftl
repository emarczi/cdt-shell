#!/bin/bash

docker run \
	--add-host "${domain}:127.0.0.1" \
	--name devon_base \
	--net=host \
	--rm \
	-v ${devon_path}:/home/rr \
	-ti devon_base:acgps "$@"
