FROM alpine:3.13.5

LABEL maintainer Endre Marczi endre.marczi@cashare.ch

ENV \
	LANG=C.UTF-8 \
	LC_CTYPE=en_GB.UTF-8

<#if os == "linux">
RUN adduser -h /home/rr -s /bin/sh -D -u ${user_id} rr
</#if>

RUN \
	apk --update add \
		curl \
		git \
		maven \
		openjdk8-jre \
		openssl \
		ssmtp \
		yarn && \
	rm -rf /var/cache/apk/*

COPY ./files/ssmtp/* /etc/ssmtp/

CMD [ "/bin/sh", "-l" ]
