<#if os == "linux">
<#assign concat = "&& \\">
</#if>
<#if os == "windows">
<#assign concat = "">
</#if>
FROM devon_base:acgps

LABEL maintainer Endre Marczi endre.marczi@cashare.ch

ENV \
	LANG=C.UTF-8 \
	LC_CTYPE=en_GB.UTF-8

RUN \
	apk --update add \
		postgresql && \
	rm -rf /var/cache/apk/* && \
	mkdir /run/postgresql ${concat}
<#if os == "linux">
	chown rr:rr /run/postgresql

USER rr

</#if>
CMD [ "/usr/bin/postgres", "-D", "/home/rr/postgres" ]
