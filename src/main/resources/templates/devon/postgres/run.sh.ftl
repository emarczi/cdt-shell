docker run \
	--add-host "${domain}:127.0.0.1" \
	--name devon_postgres \
	--net=host \
	--rm \
	-v ${devon_path}/postgres:/home/rr/postgres \
	-ti devon_postgres:latest "$@"
