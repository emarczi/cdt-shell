<#if os == "linux">
<#assign concat = "&& \\">
</#if>
<#if os == "windows">
<#assign concat = "">
</#if>
FROM devon_base:acgps

LABEL maintainer Endre Marczi endre.marczi@cashare.ch

ENV \
	LANG=C.UTF-8 \
	LC_CTYPE=en_GB.UTF-8

RUN \
	apk --update add \
		mariadb \
		mariadb-client && \
	rm -rf /var/cache/apk/*

RUN \
	mkdir /run/mysqld ${concat}
<#if os == "linux">
	chown rr:rr  /run/mysqld && \
	chown rr:rr /var/lib/mysql

USER rr
</#if>

CMD [ "mysqld", "--skip-grant-tables", "--datadir=/var/lib/mysql", "--skip_networking=0", "--port=3306", "--sql-mode=" ]
