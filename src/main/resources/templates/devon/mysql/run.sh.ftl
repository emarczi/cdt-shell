#!/bin/bash

docker run \
	--name devon_mysql \
	--net=host \
	--rm \
	-v ${devon_path}/mysql:/var/lib/mysql \
	-ti devon_mysql:latest "$@"
