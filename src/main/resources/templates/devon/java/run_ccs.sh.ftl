<#if os == "linux">
<#assign cmd = "su - rr">
</#if>
<#if os == "windows">
<#assign cmd = "/bin/sh -l">
</#if>
#!/bin/bash

docker run \
	--add-host "${domain}:127.0.0.1" \
	--name devon_java_ccs \
	--net=host \
	--rm \
	-v ${devon_path}/configuration:/home/rr/configuration \
	-v ${devon_path}/jars:/home/rr/jars \
	-v ${devon_path}/ssl:/home/rr/ssl \
	-ti devon_base:acgps ${cmd} -c "java -Dfbn=D_CCS -Xss256k -XX:+UseSerialGC -XX:MinHeapFreeRatio=10 -XX:MaxHeapFreeRatio=70 -XX:CompressedClassSpaceSize=64m -XX:ReservedCodeCacheSize=64m -XX:MaxMetaspaceSize=128m -Xms5m -Xmx100m -jar /home/rr/jars/cloud_configuration_server-1.0.0.jar"
