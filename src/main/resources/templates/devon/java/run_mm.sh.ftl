<#if os == "linux">
<#assign cmd = "su - rr">
</#if>
<#if os == "windows">
<#assign cmd = "/bin/sh -l">
</#if>
#!/bin/bash

docker run \
	--add-host "${domain}:127.0.0.1" \
	--name devon_java_mm \
	--net=host \
	--rm \
	-v ${devon_path}/jars:/home/rr/jars \
	-v ${devon_path}/ssl:/home/rr/ssl \
	-ti devon_base:acgps ${cmd} -c "java -Dfbn=D_MM -XX:+TieredCompilation -XX:TieredStopAtLevel=1 -noverify -Xss256k -XX:MinHeapFreeRatio=10 -XX:CompressedClassSpaceSize=64m -XX:ReservedCodeCacheSize=64m -XX:MaxMetaspaceSize=128m -Xms5m -Xmx1024m -jar /home/rr/jars/mm-1.0.0.jar --spring.profiles.active=cloud,mysql_jpa,ssl,local,mm"
