#!/bin/bash

docker run \
	--name devon_php \
	--net=host \
	--rm \
	-v ${devon_path}/etc/apache2.conf.d:/home/rr/etc \
	-v ${devon_path}/html:/home/rr/html \
	-v ${devon_path}/php:/home/rr/php \
	-v ${devon_path}/ssl:/home/rr/ssl \
	-ti devon_php:latest "$@"
