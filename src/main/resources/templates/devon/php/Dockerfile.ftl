FROM devon_base:acgps

LABEL maintainer Endre Marczi endre.marczi@cashare.ch

ENV \
	LANG=C.UTF-8 \
	LC_CTYPE=en_GB.UTF-8

RUN \
	apk --update add \
		apache2 \
		apache2-proxy \
		apache2-ssl \
		php7-curl \
		php7-apache2 \
		php7-calendar \
		php7-cli \
		php7-dom \
		php7-exif \
		php7-gd \
		php7-intl \
		php7-json \
		php7-mbstring \
		php7-pdo_mysql \
		php7-pdo_pgsql \
		php7-phar \
		php7-session \
		php7-tokenizer \
		php7-xml \
		php7-xmlwriter \
		php7-zip && \
	rm -rf /var/cache/apk/*

RUN \
	sed -i 's/#ServerName www.example.com:80/ServerName localhost:80/' /etc/apache2/httpd.conf && \
	sed -i "s/^#LoadModule rewrite_module modules\/mod_rewrite.so/LoadModule rewrite_module modules\/mod_rewrite.so/" /etc/apache2/httpd.conf && \
	sed -i 's/^ServerTokens OS$/ServerTokens Prod/' /etc/apache2/httpd.conf && \
	sed -i 's/^ServerSignature On$/ServerSignature Off/' /etc/apache2/httpd.conf && \
	sed -i "s/^User apache$/User rr/" /etc/apache2/httpd.conf && \
	sed -i 's/^Group apache$/Group rr/' /etc/apache2/httpd.conf && \
	echo "DirectoryIndex index.php" >> /etc/apache2/httpd.conf && \
	echo "PidFile /root/httpd.pid" >> /etc/apache2/httpd.conf && \
	echo "Include /home/rr/etc/*.conf" >> /etc/apache2/httpd.conf

COPY ./files/adminer.php /var/www/localhost/htdocs/

CMD [ "httpd", "-DSSL", "-DPHP7", "-DFOREGROUND" ]
