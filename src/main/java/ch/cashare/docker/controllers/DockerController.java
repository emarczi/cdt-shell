package ch.cashare.docker.controllers;

import ch.cashare.docker.services.DockerPackageService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.shell.standard.ShellComponent;
import org.springframework.shell.standard.ShellMethod;
import org.springframework.shell.standard.ShellOption;

/**
 *
 * @author Endre Marczi
 */
@ShellComponent
public class DockerController {

	@Autowired
	DockerPackageService dockerPackageService;

	@ShellMethod("Generate all Devon Docker packages.")
	public String generateDocker(
			@ShellOption(help = "The development domain the services run on.") String domain,
			@ShellOption(help = "The path to development files.") String devonPath,
			@ShellOption(help = "Which os do you use. Valid: linux, windows") String os,
			@ShellOption(help = "The user ID the services shall run as (on Linux: id -u [username of developer], for Windows use 0).") int userId
	) {
		String outputFilepathBase = "devon_base.zip";
		String outputFilepathJava = "devon_java.zip";
		String outputFilepathMySql = "devon_mysql.zip";
		String outputFilepathPhp = "devon_php.zip";
		String outputFilepathPostgres = "devon_postgres.zip";

		dockerPackageService.generateDockerBasePackage(devonPath, domain, os, outputFilepathBase, userId);
		dockerPackageService.generateDockerJavaPackage(devonPath, domain, os, outputFilepathJava, userId);
		dockerPackageService.generateDockerMySqlPackage(devonPath, domain, os, outputFilepathMySql, userId);
		dockerPackageService.generateDockerPhpPackage(devonPath, domain, os, outputFilepathPhp, userId);
		dockerPackageService.generateDockerPostgresPackage(devonPath, domain, os, outputFilepathPostgres, userId);

		return "The Devon Docker packages were successfully written";
	}
}
