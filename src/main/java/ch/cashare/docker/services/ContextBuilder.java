package ch.cashare.docker.services;

import org.springframework.stereotype.Component;
import java.util.HashMap;
import java.util.Map;

@Component
public class ContextBuilder {

	public Map<String, String> getDockerModel(String domain) {
		Map<String, String> model = new HashMap<>();

		model.put("domain", domain);

		return model;
	}

}
