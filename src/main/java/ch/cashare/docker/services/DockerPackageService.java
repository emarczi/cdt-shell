package ch.cashare.docker.services;

import java.io.ByteArrayInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ClassPathResource;
import org.springframework.stereotype.Service;

/**
 *
 * @author Endre Marczi
 */
@Service
public class DockerPackageService {

	@Autowired
	TemplateService templateService;

	private void addInputStreamToZip(InputStream inputStream, String entryName, ZipOutputStream zos) {
		try {
			ZipEntry zipEntry =new ZipEntry(entryName);
			zos.putNextEntry(zipEntry);
			byte[] buffer = new byte[1024];
			int len;
			while ((len = inputStream.read(buffer)) > 0) {
				zos.write(buffer, 0, len);
			}
			zos.closeEntry();
		} catch (IOException ex) {
			throw new RuntimeException("Could not write " + entryName + " to ZIP", ex);
		}
	}

	private void addStringToZip(String entryData, String entryName, ZipOutputStream zos) {
		InputStream inputStream = new ByteArrayInputStream(entryData.getBytes());
		addInputStreamToZip(inputStream, entryName, zos);
	}

	public void generateDockerBasePackage(String devonPath, String domain, String os, String outputFilepath, int userId) {
		String create_ssl_certificates_sh = templateService.renderDevonBaseCreateSslCertificates(devonPath, domain, os, userId);
		String dockerfile = templateService.renderDevonBaseDockerfile(devonPath, domain, os, userId);
		String run_sh = templateService.renderDevonBaseRun(devonPath, domain, os, userId);

		ZipOutputStream zos;
		try {
			InputStream build_sh = new ClassPathResource("templates/devon/base/build.sh").getInputStream();
			InputStream revaliases = new ClassPathResource("templates/devon/base/revaliases").getInputStream();
			InputStream ssmtp_conf = new ClassPathResource("templates/devon/base/ssmtp.conf").getInputStream();

			zos = new ZipOutputStream(new FileOutputStream(outputFilepath));
			addInputStreamToZip(build_sh, "build.sh", zos);
			addStringToZip(create_ssl_certificates_sh, "create_ssl_certificates.sh", zos);
			addStringToZip(dockerfile, "Dockerfile", zos);
			addStringToZip(run_sh, "run.sh", zos);
			addInputStreamToZip(revaliases, "files/ssmtp/revaliases", zos);
			addInputStreamToZip(ssmtp_conf, "files/ssmtp/ssmtp.conf", zos);
			zos.close();
		} catch (FileNotFoundException ex) {
			throw new RuntimeException("Could not write to " + outputFilepath, ex);
		} catch (IOException ex) {
			throw new RuntimeException("Could not write to " + outputFilepath, ex);
		}

	}

	public void generateDockerJavaPackage(String devonPath, String domain, String os, String outputFilepath, int userId) {
		String run_authserver_sh = templateService.renderDevonJavaRun(devonPath, domain, os, "run_authserver.sh.ftl", userId);
		String run_ccs_sh = templateService.renderDevonJavaRun(devonPath, domain, os, "run_ccs.sh.ftl", userId);
		String run_mm_sh = templateService.renderDevonJavaRun(devonPath, domain, os, "run_mm.sh.ftl", userId);

		ZipOutputStream zos;
		try {
			zos = new ZipOutputStream(new FileOutputStream(outputFilepath));
			addStringToZip(run_authserver_sh, "run_authserver.sh", zos);
			addStringToZip(run_ccs_sh, "run_ccs.sh", zos);
			addStringToZip(run_mm_sh, "run_mm.sh", zos);
			zos.close();
		} catch (FileNotFoundException ex) {
			throw new RuntimeException("Could not write to " + outputFilepath, ex);
		} catch (IOException ex) {
			throw new RuntimeException("Could not write to " + outputFilepath, ex);
		}

	}

	public void generateDockerMySqlPackage(String devonPath, String domain, String os, String outputFilepath, int userId) {
		String dockerfile = templateService.renderDevonMySqlDockerfile(devonPath, domain, os, userId);
		String initialise_sh = templateService.renderDevonMySqlInitialise(devonPath, domain, os, userId);
		String run_sh = templateService.renderDevonMySqlRun(devonPath, domain, os, userId);

		ZipOutputStream zos;
		try {
			InputStream build_sh = new ClassPathResource("templates/devon/mysql/build.sh").getInputStream();

			zos = new ZipOutputStream(new FileOutputStream(outputFilepath));
			addInputStreamToZip(build_sh, "build.sh", zos);
			addStringToZip(dockerfile, "Dockerfile", zos);
			addStringToZip(initialise_sh, "initialise.sh", zos);
			addStringToZip(run_sh, "run.sh", zos);
			zos.close();
		} catch (FileNotFoundException ex) {
			throw new RuntimeException("Could not write to " + outputFilepath, ex);
		} catch (IOException ex) {
			throw new RuntimeException("Could not write to " + outputFilepath, ex);
		}

	}

	public void generateDockerPhpPackage(String devonPath, String domain, String os, String outputFilepath, int userId) {
		String dockerfile = templateService.renderDevonPhpDockerfile(devonPath, domain, os, userId);
		String run_sh = templateService.renderDevonPhpRun(devonPath, domain, os, userId);

		ZipOutputStream zos;
		try {
			InputStream build_sh = new ClassPathResource("templates/devon/php/build.sh").getInputStream();

			zos = new ZipOutputStream(new FileOutputStream(outputFilepath));
			addInputStreamToZip(build_sh, "build.sh", zos);
			addStringToZip(dockerfile, "Dockerfile", zos);
			addStringToZip(run_sh, "run.sh", zos);
			zos.close();
		} catch (FileNotFoundException ex) {
			throw new RuntimeException("Could not write to " + outputFilepath, ex);
		} catch (IOException ex) {
			throw new RuntimeException("Could not write to " + outputFilepath, ex);
		}

	}

	public void generateDockerPostgresPackage(String devonPath, String domain, String os, String outputFilepath, int userId) {
		String dockerfile = templateService.renderDevonPostgresDockerfile(devonPath, domain, os, userId);
		String initialise_sh = templateService.renderDevonPostgresInitialise(devonPath, domain, os, userId);
		String run_sh = templateService.renderDevonPostgresRun(devonPath, domain, os, userId);

		ZipOutputStream zos;
		try {
			InputStream build_sh = new ClassPathResource("templates/devon/postgres/build.sh").getInputStream();

			zos = new ZipOutputStream(new FileOutputStream(outputFilepath));
			addInputStreamToZip(build_sh, "build.sh", zos);
			addStringToZip(dockerfile, "Dockerfile", zos);
			addStringToZip(initialise_sh, "initialise.sh", zos);
			addStringToZip(run_sh, "run.sh", zos);
			zos.close();
		} catch (FileNotFoundException ex) {
			throw new RuntimeException("Could not write to " + outputFilepath, ex);
		} catch (IOException ex) {
			throw new RuntimeException("Could not write to " + outputFilepath, ex);
		}

	}
}
