package ch.cashare.docker.services;

import freemarker.core.ParseException;
import freemarker.template.Configuration;
import freemarker.template.MalformedTemplateNameException;
import freemarker.template.Template;
import freemarker.template.TemplateException;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.ui.freemarker.FreeMarkerTemplateUtils;

/**
 *
 * @author Endre Marczi
 */
@Service
public class TemplateService {

	@Autowired
	private Configuration freemarkerConfig;

	private String renderTemplate(String devonPath, String domain, String os, String template, int userId) {
		Map<String, String> model = new HashMap<>();
		model.put("devon_path", devonPath);
		model.put("domain", domain);
		model.put("os", os);
		model.put("user_id", userId + "");

		return render(model, template);
	}

	public String renderDevonBaseCreateSslCertificates(String devonPath, String domain, String os, int userId) {
		return renderTemplate(devonPath, domain, os, "/devon/base/create_ssl_certificates.sh.ftl", userId);
	}

	public String renderDevonBaseDockerfile(String devonPath, String domain, String os, int userId) {
		return renderTemplate(devonPath, domain, os, "/devon/base/Dockerfile.ftl", userId);
	}

	public String renderDevonBaseRun(String devonPath, String domain, String os, int userId) {
		return renderTemplate(devonPath, domain, os, "/devon/base/run.sh.ftl", userId);
	}

	public String renderDevonJavaRun(String devonPath, String domain, String os, String template, int userId) {
		return renderTemplate(devonPath, domain, os, "/devon/java/" + template, userId);
	}

	public String renderDevonMySqlDockerfile(String devonPath, String domain, String os, int userId) {
		return renderTemplate(devonPath, domain, os, "/devon/mysql/Dockerfile.ftl", userId);
	}

	public String renderDevonMySqlInitialise(String devonPath, String domain, String os, int userId) {
		return renderTemplate(devonPath, domain, os, "/devon/mysql/initialise.sh.ftl", userId);
	}

	public String renderDevonMySqlRun(String devonPath, String domain, String os, int userId) {
		return renderTemplate(devonPath, domain, os, "/devon/mysql/run.sh.ftl", userId);
	}

	public String renderDevonPhpDockerfile(String devonPath, String domain, String os, int userId) {
		return renderTemplate(devonPath, domain, os, "/devon/php/Dockerfile.ftl", userId);
	}

	public String renderDevonPhpRun(String devonPath, String domain, String os, int userId) {
		return renderTemplate(devonPath, domain, os, "/devon/php/run.sh.ftl", userId);
	}

	public String renderDevonPostgresDockerfile(String devonPath, String domain, String os, int userId) {
		return renderTemplate(devonPath, domain, os, "/devon/postgres/Dockerfile.ftl", userId);
	}

	public String renderDevonPostgresInitialise(String devonPath, String domain, String os, int userId) {
		return renderTemplate(devonPath, domain, os, "/devon/postgres/initialise.sh.ftl", userId);
	}

	public String renderDevonPostgresRun(String devonPath, String domain, String os, int userId) {
		return renderTemplate(devonPath, domain, os, "/devon/postgres/run.sh.ftl", userId);
	}

	private String render(Map<String, String> model, String filepath) {
		try {
			Template template = freemarkerConfig.getTemplate(filepath);
			return FreeMarkerTemplateUtils.processTemplateIntoString(template, model);
		} catch (MalformedTemplateNameException ex) {
			throw new RuntimeException("Template was not rendered: " + filepath, ex);
		} catch (ParseException | TemplateException ex) {
			throw new RuntimeException("Template was not rendered: " + filepath, ex);
		} catch (IOException ex) {
			throw new RuntimeException("Template was not rendered: " + filepath, ex);
		}
	}
}
